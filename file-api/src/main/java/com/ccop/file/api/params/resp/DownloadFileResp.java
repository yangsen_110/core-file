package com.ccop.file.api.params.resp;

import com.sun.istack.internal.NotNull;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * com.ccop.file.api.params.req.DownloadFileReq
 *
 * 文件下载响应类
 *
 * @author yangsen
 * 2019/12/4 6:03 下午
 */
@Getter
@Setter
@ApiModel(description = "文件下载响应类")
public class DownloadFileResp {

    /**
     * 文件字节流
     */
    @ApiModelProperty(value = "文件字节流")
    private byte[] file;
}
