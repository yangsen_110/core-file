package com.ccop.file.api.params.req;

import com.sun.istack.internal.NotNull;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * com.ccop.file.api.params.req.DownloadFileReq
 *
 * 文件删除请求类
 *
 * @author yangsen
 * 2019/12/4 6:03 下午
 */
@Getter
@Setter
@ApiModel(description = "文件删除请求类")
public class DeleteFileReq {

    /**
     * 文件相对路径
     */
    @ApiModelProperty(value = "文件相对路径", required = true)
    @NotNull
    private String relativeFilePath;
}
