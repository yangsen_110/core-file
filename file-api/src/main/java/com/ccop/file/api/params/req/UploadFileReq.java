package com.ccop.file.api.params.req;


import com.sun.istack.internal.NotNull;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * com.ccop.file.api.params.req.FileUploadReq
 *
 *  文件上传请求类
 *
 * @author yangsen
 * 2019/12/2 2:34 下午
 */
@Getter
@Setter
@ApiModel(description = "文件上传请求类")
public class UploadFileReq {

    /**
     * 请求标识
     */
    @ApiModelProperty(value = "请求标识", required = true)
    @NotNull
    private String requestId;

    /**
     * 模块
     */
    @ApiModelProperty(value = "模块", required = true)
    @NotNull
    private String module;

    /**
     * 目标路径
     */
    @ApiModelProperty(value = "目标路径", required = true)
    @NotNull
    private String dstPath;

    /**
     * 文件类型
     */
    @ApiModelProperty(value = "文件类型", required = true)
    @NotNull
    private String fileType;

    /**
     * 文件名
     */
    @ApiModelProperty(value = "文件名", required = true)
    @NotNull
    private String fileName;

    /**
     * 文件字节码
     */
    @ApiModelProperty(value = "文件字节码", required = true)
    @NotNull
    private byte[] file;

}
