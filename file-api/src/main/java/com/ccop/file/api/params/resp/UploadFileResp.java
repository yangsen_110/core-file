package com.ccop.file.api.params.resp;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

/**
 * com.ccop.file.api.params.req.FileUploadResp
 *
 * 文件上传响应类
 *
 * @author yangsen
 * 2019/12/2 2:35 下午
 */
@Getter
@Setter
@ApiModel(description = "文件上传响应类")
public class UploadFileResp {
    /**
     * 请求标识
     */
    private String requestId;

    /**
     * 文件路径
     */
    private String filePath;
}
