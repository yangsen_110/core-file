package com.ccop.file.service;

import com.ccop.file.api.params.req.DeleteFileReq;
import com.ccop.file.api.params.req.DownloadFileReq;
import com.ccop.file.api.params.req.UploadFileReq;
import com.ccop.file.api.params.resp.DownloadFileResp;
import com.ccop.file.api.params.resp.UploadFileResp;

/**
 * com.ccop.file.service.IFileService
 *
 * 文件操作接口
 *
 * @author yangsen
 * 2019/12/2 2:22 下午
 */
public interface IFileService {

    /**
     * 上传文件
     * @param uploadFileReq 文件上传对象
     */
    UploadFileResp uploadFile(UploadFileReq uploadFileReq);


    /**
     * 下载文件
     * @param downloadFileReq 文件下载请求对象
     */
    DownloadFileResp downloadFile(DownloadFileReq downloadFileReq);


    /**
     * 删除文件
     * @param deleteFileReq 文件删除对象
     */
    boolean deleteFile(DeleteFileReq deleteFileReq);

}
