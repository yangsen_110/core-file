package com.ccop.file.service;

import com.ccop.file.api.params.req.DeleteFileReq;
import com.ccop.file.api.params.req.DownloadFileReq;
import com.ccop.file.api.params.req.UploadFileReq;
import com.ccop.file.api.params.resp.DownloadFileResp;
import com.ccop.file.api.params.resp.UploadFileResp;
import com.ccop.file.config.FileConfiguration;
import com.cia.framework.core.sdk.util.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.URI;
import java.text.MessageFormat;

/**
 * com.ccop.file.HDFSService
 * HDFS文件操作实现
 *
 * @author yangsen
 * 2019/12/2 2:09 下午
 */
@Slf4j
@Service
public class HDFSFileService implements IFileService {


    @Autowired
    private FileConfiguration fileConfiguration;

    @Autowired
    private FileConfiguration.Hdfs hdfs;

    @Autowired
    private Configuration configuration;

    private ThreadLocal<FileSystem> fileSystemThreadLocal = new ThreadLocal<>();

    @Override
    public UploadFileResp uploadFile(UploadFileReq uploadFileReq) {

        UploadFileResp uploadFileResp = new UploadFileResp();
        uploadFileResp.setRequestId(uploadFileReq.getRequestId());
        // 文件字节数组
        byte[] fileBytes = uploadFileReq.getFile();
        // 文件长度
        int length = uploadFileReq.getFile().length;
        if (length > fileConfiguration.getMaxSize()) {
            throw new RuntimeException(MessageFormat.format("upload file error-file is too large. max size is {}", fileConfiguration.getMaxSize().toString()));
        }
        // 目标路径
        String dstPath = uploadFileReq.getDstPath();
        FileSystem fileSystem = null;
        InputStream in;
        OutputStream out;
        try {
            fileSystem = this.getFileSystem();
            // 目标文件绝对路径
            String absoluteFilePath = absoluteFilePath(uploadFileReq.getModule(), dstPath, uploadFileReq.getFileName());
            Path path = new Path(absoluteFilePath);
            /* 创建父级目录 */
            boolean mkdirResult = this.mkdir(path.getParent().toString());

            if (!mkdirResult) {
                throw new RuntimeException(MessageFormat.format("upload file error-create dictionary error-{}", absoluteFilePath));
            }

            // 同名文件覆盖
            out = fileSystem.create(path, this.fileConfiguration.isOverwrite());
            in = new ByteArrayInputStream(fileBytes, 0, length);
            /* 输出流写入 */
            this.writeInternal(in, out);
            // 设置绝对
            uploadFileResp.setFilePath(absoluteFilePath);
        } catch (IOException e) {
            log.error("upload file error", e);
            throw new RuntimeException("upload file error", e);
        } finally {
            this.close(fileSystem);
            fileSystemThreadLocal.remove();
        }
        return uploadFileResp;
    }

    @Override
    public DownloadFileResp downloadFile(DownloadFileReq downloadFileReq) {

        FileSystem fileSystem;
        DownloadFileResp downloadFileResp = new DownloadFileResp();
        // 文件绝对
        String absoluteFilePath = downloadFileReq.getRelativeFilePath();
        try {
            // 目标绝对文件路径
            Path absolutePath = new Path(hdfs.getDefaultFS() + "/" + absoluteFilePath);
            fileSystem = this.getFileSystem();
            // 文件写入流
            InputStream in = fileSystem.open(absolutePath);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            out.close();
            /* 输出流写入 */
            this.writeInternal(in, out);
            byte[] file = out.toByteArray();

            downloadFileResp.setFile(file);
        } catch (IOException e) {
            log.error(MessageFormat.format("download file error, absoluteFilePath:{}", absoluteFilePath), e);
        } finally {
            fileSystemThreadLocal.remove();
        }

        return downloadFileResp;
    }

    @Override
    public boolean deleteFile(DeleteFileReq deleteFileReq) {

        FileSystem fileSystem;
        // 文件绝对
        String absoluteFilePath = deleteFileReq.getRelativeFilePath();
        try {
            // 目标文件绝对路径
            Path absolutePath = new Path(hdfs.getDefaultFS() + "/" + absoluteFilePath);
            fileSystem = this.getFileSystem();

            return fileSystem.delete(absolutePath, false);
        } catch (IOException e) {
            log.error(MessageFormat.format("download file error, absoluteFilePath:{}", absoluteFilePath), e);
        } finally {
            fileSystemThreadLocal.remove();
        }

        return false;
    }


    /**
     * 输出流写入
     *
     * @param in  输入流
     * @param out 输出流
     */
    private void writeInternal(InputStream in, OutputStream out) throws IOException {
        int bufferSize = 1024;
        byte[] buffer = new byte[bufferSize];
        try {
            int bytesRead = in.read(buffer);
            while (bytesRead != -1) {
                out.write(buffer, 0, bytesRead);
                bytesRead = in.read(buffer);
            }
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    log.warn("upload file closing OutputStream error-{}", e.getMessage());
                }
            }
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    log.warn("upload file closing InputStream error-{}", e.getMessage());
                }
            }
        }
    }

    /**
     * 创建HDFS目录
     *
     * @param path HDFS的相对目录路径，比如：/testDir
     * @author adminstrator
     */
    private boolean mkdir(String path) {
        //如果目录已经存在，则直接返回
        if (checkExists(path)) {
            return true;
        } else {
            FileSystem fileSystem = this.getFileSystem();
            try {
                //创建目录
                return fileSystem.mkdirs(new Path(path));
            } catch (IOException e) {
                log.error(MessageFormat.format("create hdfs dictionary error，path:{}", path), e);
                return false;
            }
        }
    }

    /**
     * 判断文件或者目录是否在HDFS上面存在
     *
     * @param path HDFS的相对目录路径，比如：/testDir、/testDir/a.txt
     * @return boolean true: 存在；false：不存在
     */
    private boolean checkExists(String path) {

        FileSystem fileSystem;
        try {
            fileSystem = this.getFileSystem();
            // 创建目录
            return fileSystem.exists(new Path(path));
        } catch (IOException e) {
            log.error(MessageFormat.format("'判断文件或者目录是否在HDFS上面存在'失败，path:{}", path), e);
            return false;
        }
    }

    /**
     * close方法
     */
    private void close(FileSystem fileSystem) {
        if (fileSystem != null) {
            try {
                fileSystem.close();
            } catch (IOException e) {
                log.warn("closing FileSystem error-{}", e.getMessage());
            }
        }
    }




    /**
     * 文件绝对
     *
     * @param module  模块 模块作为文件路径上一级
     * @param dstPath 绝对，比如：/data
     * @return 绝对路径
     */
    private String absoluteFilePath(String module, String dstPath, String fileName) {

        // 绝对路径
        StringBuilder absolutePath = new StringBuilder();
        absolutePath.append(fileConfiguration.getRootPath());
        absolutePath.append("/");
        absolutePath.append(module);

        if (dstPath.startsWith("/")) {
            absolutePath.append(dstPath);
        } else {
            absolutePath.append("/");
            absolutePath.append(dstPath);
        }
        absolutePath.append("/");
        absolutePath.append(fileName);
        return absolutePath.toString();
    }

    /**
     * 获取HDFS文件系统
     *
     * @return HDFS文件系统
     */
    private FileSystem getFileSystem() {

        if (fileSystemThreadLocal.get() == null) {
            FileSystem fileSystem;
            try {
                if (StringUtil.isBlank(hdfs.getLoginName())) {
                    fileSystem = FileSystem.get(URI.create(hdfs.getDefaultFS()), configuration);
                } else {
                    fileSystem = FileSystem.get(new URI(hdfs.getDefaultFS()), configuration, hdfs.getLoginName());
                }
                if (fileSystem == null) {
                    log.error("get hdfs FileSystem get error");
                    throw new RuntimeException("get hdfs FileSystem error");
                }
                fileSystemThreadLocal.set(fileSystem);
            } catch (Exception e) {
                log.error("get hdfs FileSystem get error", e);
                throw new RuntimeException("get hdfs FileSystem error", e);
            }
        }

        return fileSystemThreadLocal.get();
    }
}
