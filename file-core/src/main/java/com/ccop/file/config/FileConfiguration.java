package com.ccop.file.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * com.ccop.file.config.HDFSConfiguration
 * HDFS配置
 *
 * @author yangsen
 * 2019/12/2 6:46 下午
 */
@Component
@Configuration
@ConfigurationProperties(prefix = "file")
@Getter
@Setter
public class FileConfiguration {

    /**
     * 是否覆盖文件
     */
    private boolean overwrite;

    /**
     * 根目录
     */
    private String rootPath;

    /**
     * hdfs配置信息
     */
    private Hdfs hdfs;

    /**
     * 文件最大大小 单位：byte
     */
    private Integer maxSize;


    @Getter
    @Setter
    public static class Hdfs {

        /**
         * hdfs文件系统地址
         */
        private String defaultFS;

        /**
         * 登录用户名
         */
        private String loginName;
    }

    @Bean
    public Hdfs hdfs() {

        return this.hdfs;
    }
    @Bean
    public org.apache.hadoop.conf.Configuration configuration() {

        org.apache.hadoop.conf.Configuration configuration = new org.apache.hadoop.conf.Configuration();
        configuration.set("fs.defaultFS", this.hdfs.defaultFS);
        return configuration;
    }


}


