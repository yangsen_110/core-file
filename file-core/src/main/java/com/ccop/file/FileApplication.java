package com.ccop.file;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * com.ccop.file.config.FileApplication
 *
 * 启动类
 *
 * @author yangsen
 * 2019/11/29 3:31 下午
 */
@SpringBootApplication
public class FileApplication {

    public static void main(String[] args) {
        SpringApplication.run(FileApplication.class, args);
    }
}

