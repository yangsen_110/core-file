package com.ccop.file.api.impl;

import com.ccop.file.api.params.interfaces.IFileServiceSV;
import com.ccop.file.api.params.req.DeleteFileReq;
import com.ccop.file.api.params.req.DownloadFileReq;
import com.ccop.file.api.params.req.UploadFileReq;
import com.ccop.file.api.params.resp.DownloadFileResp;
import com.ccop.file.api.params.resp.UploadFileResp;
import com.ccop.file.service.IFileService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * com.ccop.file.api.impl.FileServiceSVImpl
 * <p>
 * 文件操作实现
 *
 * @author yangsen
 * 2019/12/5 10:44 上午
 */
public class FileServiceSVImpl implements IFileServiceSV {

    @Autowired
    private IFileService fileService;

    @Override
    public UploadFileResp uploadFile(UploadFileReq uploadFileReq) {

        return this.fileService.uploadFile(uploadFileReq);
    }

    @Override
    public DownloadFileResp downloadFile(DownloadFileReq downloadFileReq) {
        return this.downloadFile(downloadFileReq);
    }

    @Override
    public boolean deleteFile(DeleteFileReq deleteFileReq) {
        return this.fileService.deleteFile(deleteFileReq);
    }
}
